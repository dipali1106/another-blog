<!-- File: /app/View/Posts/add.ctp -->
<?php echo $this->Session->flash(); ?>
<h1>Add Post</h1>

<?php
echo $this->Form->create('Post');
echo $this->Form->input('title',array('required'=>True));
echo $this->Form->input('body', array('rows' => '3'));
echo $this->Form->end('Save Post');
?>