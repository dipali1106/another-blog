<?php

class PostsController extends AppController {
	
    public $helpers = array('Html', 'Form','Session');
	public $components = array('Session');

    public function index() {
         $this->set('posts', $this->Post->find('all'));
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
    }
    public function add() {
        if ($this->request->is('post')) {
//pr();die;
        	if(!empty($this->request->data['Post']['title'])){
				$this->Post->create();
	            if ($this->Post->save($this->request->data)) {
	                //$this->Flash->success(__('Your post has been saved.'));
	                 $this->Session->setFlash('Your post has been saved.');
	                return $this->redirect(array('action' => 'index'));
	            }
        	}else{ 
        			$this->Session->setFlash('Unable to add your post.');
        	}
            
            
        }
    }
    public function edit($id = null) {
    if (!$id) {
       // throw new NotFoundException(__('Invalid post'));
    }

    $post = $this->Post->findById($id);
    //pr($post);die;
    if (!$post) {
       // throw new NotFoundException(__('Invalid post'));
    }

    if (!empty($this->request->data)) {

    	//pr($this->request->data);die;
        $this->Post->id = $id;
        if ($this->Post->save($this->request->data)) {
            $this->Session->setFlash('Your post has been updated.');
            return $this->redirect(array('action' => 'index'));
        }
       // $this->Flash->error(__('Unable to update your post.'));
    }

    if (!$this->request->data) {  //pr($post);die;
        $this->request->data = $post;
    }
}


	public function delete($id) {
		$this->layout = false;;
	if(!empty($id)){
		 
		if ($this->Post->deleteAll(array('Post.id'=>$id))) {
	        
	        $this->Session->setFlash('The post with id:'.$id.'   has been deleted.' );
	    }
	}else{
		
	             $this->Session->setFlash('The post with id: %s could not be deleted.');
	}
  return $this->redirect(array('action' => 'index'));
}
}

 
?>